import PackageDescription

let package = Package(
    name: "BeimaxConnectionPool",
    dependencies: [
        .Package(url: "https://github.com/Jasonbit/YamlSwift.git", majorVersion: 0, minor: 1),
        .Package(url: "https://github.com/Zewo/PostgreSQL.git", majorVersion: 0, minor: 1)
      ]
)
